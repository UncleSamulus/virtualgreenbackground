# VirtualGreenBackground

Python OpenCV / TensorFlow background subtraction for live webcam.

## Requirements

### System packages

* v4l2loopback
* git
* docker / docker-compose (optional)
* python3
* cuda

### Pip packages

* tf-bodypix
* tensorflow
* numpy
* opencv-python
* pyfakewebcam


### Example for OpenSUSE

### From package (non working in my situation)

```bash
sudo zypper source-install v4l2loopback
```

### From source

```
# Install kernel headers
sudo zypper in kernel-source
# Update header links
sudo reboot
# Clone v4l2loopback repository
cd /opt
git clone https://github.com/umlaeute/v4l2loopback.git
cd v4l2loopback
# Build
make
# Install
sudo make install
```

## Ressources

* [Un fond virtuel pour votre Webcam.](https://www.erroussafi.com/index.php/2020/05/30/projet-opencv-et-tensorflow-un-fond-virtuel-pour-votre-webcam/) (FR)

## Configure virtual webcam

```bash
sudo modprobe -r v4l2loopback
sudo modprobe v4l2loopback devices=1 video_nr=20 card_label="v4l2loopback" exclusive_caps=1
```

## Usage
```bash
python3 -m venv venv
source venv/bin/activate

python3 -m pip install -U pip
python3 -m pip install pyfakewebcam pyyaml opencv-python numpy 
python3 -m pip install tf-bodypix[all]
python3 src/main.py
```

### With systemd service (recommended)

```bash
sudo cp fakebg.service /etc/systemd/system/fakebg.service
sudo systemctl daemon-reload
sudo systemctl start fakebg.service
```
### Set background

The background type and src is specified in `config/config.yaml`.
