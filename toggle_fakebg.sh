#!/bin/bash
state=`systemctl is-active fakebg.service`

if [ $state = "active" ]
then
    systemctl stop fakebg.service
else
    systemctl start fakebg.service
fi