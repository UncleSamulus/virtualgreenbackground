#!/usr/bin/env python3

import tensorflow as tf
from tf_bodypix.api import download_model, load_model, BodyPixModelPaths
from cv2 import cv2
import pyfakewebcam
import numpy as np
import time
import os
import yaml

height, width = 720, 1280

# Read config
with open("config/config.yaml", 'r') as f:
    config = yaml.safe_load(f)

BG_IS_VIDEO = config['fakewebcam']['bg_type'] == "video"
BG_SRC = config['fakewebcam']['bg_src']

if BG_IS_VIDEO:
    bg_cap = cv2.VideoCapture(BG_SRC)
    bg_cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    bg_cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
    bg_cap.set(cv2.CAP_PROP_FPS, 60)
else:
    # Import background image
    replacement_bg_raw = cv2.imread(BG_SRC)
    # Resize background image
    replacement_bg = cv2.resize(replacement_bg_raw, (width, height))  

# Create virtual camera device
os.system('sudo modprobe -r v4l2loopback')
os.system('sudo modprobe v4l2loopback devices=1 video_nr=20 card_label="v4l2loopback" exclusive_caps=1')

bodypix_model = load_model(download_model(
    BodyPixModelPaths.MOBILENET_FLOAT_50_STRIDE_16
))

Capture = cv2.VideoCapture('/dev/video1')
Capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
Capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
Capture.set(cv2.CAP_PROP_FPS, 60)

fake = pyfakewebcam.FakeWebcam('/dev/video20', width, height)

def get_mask(frame):
    result = bodypix_model.predict_single(frame)
    mask = result.get_mask(threshold=0.75)
    mask = np.squeeze(mask, axis=2)
    return mask

if BG_IS_VIDEO:
    frame_number = 0
    frame_amount = bg_cap.get(cv2.CAP_PROP_FRAME_COUNT)

while True:
    if BG_IS_VIDEO:
        frame_number = (frame_number + 1) % frame_amount
        bg_cap.set(cv2.CAP_PROP_POS_FRAMES, frame_number-1)
        success, replacement_bg = bg_cap.read()
        replacement_bg = cv2.resize(replacement_bg, (width, height))  

    success, frame = Capture.read()
    if (not success):
        print("Error: cannot get frame")
        continue    
    mask = get_mask(frame)
    # magic part !
    inv_mask = 1-mask
    for c in range(frame.shape[2]):
        frame[:,:,c] = frame[:,:,c]*mask + replacement_bg[:,:,c]*inv_mask
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    fake.schedule_frame(frame)
